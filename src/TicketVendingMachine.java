import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.Calendar;
import java.util.Objects;

public class TicketVendingMachine {

    Calendar calendar = Calendar.getInstance();

    String currentTextItems;
    String currentTextPrices;
    int total;

    int year = calendar.get(Calendar.YEAR);
    int month = calendar.get(Calendar.MONTH) + 1;
    int day = calendar.get(Calendar.DATE);
    int hour = calendar.get(Calendar.HOUR_OF_DAY);
    int minute = calendar.get(Calendar.MINUTE);
    int second = calendar.get(Calendar.SECOND);
    int week = calendar.get(Calendar.DAY_OF_WEEK) - 1;

    private JPanel root;
    private JButton btnCheckOut;
    private JPanel pnlTotal;
    private JTextPane textpaneOrderedItems;
    private JTextPane textpanePrice;
    private JTextField textfieldTotal;
    private JTextPane footer;
    private JTabbedPane tbMenu;
    private JPanel tbFood;
    private JPanel tbDrink;
    private JPanel tbTimeMenu;
    private JButton btnRice;
    private JButton btnMisosoup;
    private JButton btnKinpiraburdock;
    private JButton btnKaraage;
    private JButton btnTempura;
    private JButton btnSashimi;
    private JButton btnCoke;
    private JButton btnOrangejuice;
    private JButton btnGreentea;
    private JButton btnChangingMenu1;
    private JButton btnChangingMenu2;
    private JButton btnChangingMenu3;
    private JButton btnCoffee;
    private JButton btnGingerFriedPork;
    private JButton btnFriedcartilage;
    private JButton btnMuffin;
    private JButton btnTeriyaki;
    private JButton btnEdamame;

    public TicketVendingMachine() {
        /***アイテムリストの各パネルの編集を無効にする***/
        textpaneOrderedItems.setEditable(false);
        textpanePrice.setEditable(false);
        textfieldTotal.setEditable(false);

        /***フッターの設定をし、編集不可にする***/
        footer.setText("Time Menu\n" +
                " 7:00 - 11:00\tMorning Menu  [ Coffee , Muffin ]\n" +
                "11:00 - 14:00\tLunch Menu    [ Ginger Fried Pork , Teriyaki ]\n" +
                "17:00 - 20:00\tDinner Menu   [ Fried cartilage , Edamame ]\n\n" +
                "Note\n" +
                "・Alcohol service is available from 5:00 PM until 5:00 AM the following morning.\n"+
                "・There may be occasions when we ask for identification to verify your age before serving alcohol.\n"
        );
        footer.setEditable(false);

        /***時間によってタブを 有効/無効 にする***/
        tbMenu.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                if(hour>=7 && hour<11) {
                    tbMenu.setEnabledAt(2,true);
                }
                else if(hour>=11 && hour<14) {
                    tbMenu.setEnabledAt(2,true);
                }
                else if(hour>=17 && hour<20) {
                    tbMenu.setEnabledAt(2,true);
                }
                else{
                    tbMenu.setEnabledAt(2, false);
                }
            }
        });

        btnRice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Rice",250);
            }
        });
        btnMisosoup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Miso soup",200);
            }
        });
        btnKinpiraburdock.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kinpira burdock",180);
            }
        });
        btnKaraage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Karaage",350);
            }
        });
        btnTempura.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",500);
            }
        });
        btnSashimi.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sashimi",800);
            }
        });
        btnCoke.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Coke",120);
            }
        });
        btnOrangejuice.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Orange juice",110);
            }
        });
        btnGreentea.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Green tea",100);
            }
        });

        /***酒類提供は17:00-5:00であるためその時間以外はボタンをソフトドリンクにする***/
        if(hour>=17 || (hour>=0 && hour<5)) {
            /**ボタンの設定**/
            btnChangingMenu1.setIcon(new ImageIcon(
                    Objects.requireNonNull(this.getClass().getResource("beer.jpg"))
            ));
            setBtnText(btnChangingMenu1,"Beer",320);
            btnChangingMenu2.setIcon(new ImageIcon(
                    Objects.requireNonNull(this.getClass().getResource("lemon_sour.jpg"))
            ));
            setBtnText(btnChangingMenu2,"Lemon sour",300);
            btnChangingMenu3.setIcon(new ImageIcon(
                    Objects.requireNonNull(this.getClass().getResource("shirakawagou.jpg"))
            ));
            setBtnText(btnChangingMenu3,"Shirakawagou",400);
            /***機能の設定***/
            btnChangingMenu1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Beer",320);
                }
            });
            btnChangingMenu2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Lemon sour",300);
                }
            });
            btnChangingMenu3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Shirakawagou",400);
                }
            });
        }
        else {
            /**ボタンの設定**/
            btnChangingMenu1.setIcon(new ImageIcon(
                    Objects.requireNonNull(this.getClass().getResource("gingerale.jpg"))
            ));
            setBtnText(btnChangingMenu1,"Gingerale",130);
            btnChangingMenu2.setIcon(new ImageIcon(
                    Objects.requireNonNull(this.getClass().getResource("applejuice.jpg"))
            ));
            setBtnText(btnChangingMenu2,"Apple juice",140);
            btnChangingMenu3.setIcon(new ImageIcon(
                    Objects.requireNonNull(this.getClass().getResource("oolongtea.jpg"))
            ));
            setBtnText(btnChangingMenu3,"Oolong tea",160);
            /***機能の設定***/
            btnChangingMenu1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Gingerale",130);
                }
            });
            btnChangingMenu2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Apple juice",140);
                }
            });
            btnChangingMenu3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Oolong tea",160);
                }
            });
        }

        /***時間によって各Time Menuのボタンを　有効/無効 にする***/
        if(hour>=7 && hour<11) {
            btnCoffee.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Coffee",150);
                }
            });
            btnMuffin.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Muffin",90);
                }
            });
        }
        else {
            btnCoffee.setEnabled(false);
            btnMuffin.setEnabled(false);
        }
        if(hour>=11 && hour<14) {
            btnGingerFriedPork.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Ginger Fried Pork",280);
                }
            });
            btnTeriyaki.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Teriyaki",340);
                }
            });
        }
        else {
            btnGingerFriedPork.setEnabled(false);
            btnTeriyaki.setEnabled(false);
        }
        if(hour>=17 && hour<=20) {
            btnFriedcartilage.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Fried cartilage",220);
                }
            });
            btnEdamame.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    order("Edamame",240);
                }
            });
        }
        else{
            btnFriedcartilage.setEnabled(false);
            btnEdamame.setEnabled(false);
        }

        btnCheckOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(
                        null,
                        "Woud you like to checkout ?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION
                );
                if(confirmation==0){
                    JOptionPane.showMessageDialog(
                            null,
                            "Thank you.\nThe total is " + total + "yen."
                    );
                    total=0;
                    /***アイテムリストの各パネルの編集を有効する***/
                    textpaneOrderedItems.setEditable(true);
                    textpanePrice.setEditable(true);
                    textfieldTotal.setEditable(true);
                    /***Ordered Itemsを編集する***/
                    textpaneOrderedItems.setText("");
                    textpanePrice.setText("");
                    textfieldTotal.setText("");
                    /***アイテムリストの各パネルの編集を無効にする***/
                    textpaneOrderedItems.setEditable(false);
                    textpanePrice.setEditable(false);
                    textfieldTotal.setEditable(false);
                }
            }
        });
    }

    void setBtnText(JButton btn, String name, int price){
        btn.setText(name + "  " + price + "yen");
        btn.setHorizontalTextPosition(JButton.CENTER);
        btn.setVerticalTextPosition(JButton.BOTTOM);
    }

    void order(String item, int price) {
        int confirmation = JOptionPane.showConfirmDialog(
                null,
                "Would you like to order " + item +" ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION
        );
        if(confirmation == 0) {
            total+=price;
            JOptionPane.showMessageDialog(
                    null,
                    "Order for " + item + " received"
            );
            /***アイテムリストの各パネルの編集を有効する***/
            textpaneOrderedItems.setEditable(true);
            textpanePrice.setEditable(true);
            textfieldTotal.setEditable(true);
            /***Ordered Itemsを編集する***/
            currentTextItems=textpaneOrderedItems.getText();
            textpaneOrderedItems.setText(currentTextItems + item + "\n");
            currentTextPrices=textpanePrice.getText();
            textpanePrice.setText(currentTextPrices + price +"yen" + "\n");
            textfieldTotal.setText("Total\t\t\t" + total + " yen");
            /***アイテムリストの各パネルの編集を無効にする***/
            textpaneOrderedItems.setEditable(false);
            textpanePrice.setEditable(false);
            textfieldTotal.setEditable(false);
        }
    }
    public static void main(String[] args) {
        JFrame frame = new JFrame("TicketVendingMachine");
        frame.setContentPane(new TicketVendingMachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
